API Level Tests for GASP - Games Module
=======================================

games.py
--------

To import games, use:

  >>> from gasp import games

To use RGB color values in GASP games:

  >>> from gasp import color

To start a graphics window, create a `Screen` object:

  >>> screen = games.Screen(width=800, height=800, title="Robots")

And set a background color:

  >>> screen.set_background_color(color.YELLOW)

To clear the screen use the `clear` method:

  >>> screen.clear()

Let's draw some circles with `Circle` objects:

  >>> red_circle = games.Circle(screen, 200, 200, radius=50, color=color.RED)
  >>> blue_circle = games.Circle(screen, 400, 400, radius=50, color=color.BLUE)
  >>> green_circle = games.Circle(screen, 600, 600, radius=50, color=color.GREEN)

And modify them:

  >>> red_circle.move_to(100, 100)
  >>> blue_circle.move_by(300, -200)
  >>> green_circle.move_to(500, 500)
  >>> red_circle.set_radius(20)

Write something down with `Text`:

  >>> text = games.Text(screen, 300, 300, "Hello World!", size=35, color=color.BLACK)
  >>> text.set_text("^ Close Me ^")
  >>> text.move_to(400, 25)
  >>> text.get_text()
  '^ Close Me ^'

Draw a shape with `Polygon`. Define its shape with a list of points relative to each other.
Let's make a box:

  >>> shape = [(0, 0), (0, 50), (50, 50), (50, 0)]
  >>> box = games.Polygon(screen, 500, 500, shape, color.PURPLE)

This should make it that the screen auto closes for the purposes of this doctest.
It is not recommended you re-assign GASP methods in this way.

  >>> screen.tick = screen.quit

Finally, call `mainloop()` to run your creation.

  >>> screen.mainloop()

| If you see a funky looking window with equally funky geometry, great!
| Everything seemed to have worked.
| If your terminal is empty after running the tests, rest easy knowing GASP is perfect.
| If you see a test failure, however, please notify someone who can fix the issue.
